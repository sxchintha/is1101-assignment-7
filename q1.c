#include <stdio.h>

#define SIZE 100
int letterCount(char *sentence);
char getReverse(char *sentence);

int main()
{
	char sentence[SIZE];

	printf("Please enter the sentences: ");
	fgets(sentence, SIZE, stdin);
	getReverse(sentence);

	return 0;
}

int letterCount(char *sentence)
{
	int count=0;
   	while (sentence[count] != '\0')
    	count++;

	return count;
}

char getReverse(char *sentence)
{
	int n=letterCount(sentence);
	char temp[n];
	int i=0;

	while(n>0)
	{
		temp[i]=sentence[n-1];
		i++;
		n--;
	}

	temp[i]='\0';
	printf("Reverse sentence: %s\n", temp);

}

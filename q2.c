#include <stdio.h>

char string[100]="WriteaprogramtofindtheFrequencyofagivencharacterinagivenstring";
char letter[1]="g";

int letterCount()
{
	int count=0, c=0;
   	while (string[count] != '\0')
	{
		if(string[count]==letter[0])
  			c++;
    		
		count++;
	}

	return c;
}

int main()
{

	printf("Frequency of the letter: %d\n", letterCount() );

	return 0;
}
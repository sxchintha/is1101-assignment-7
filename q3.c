#include <stdio.h>

void getAdd();
void getMultiply();

int matrix1[3][3]={
	{1,5,9},
	{7,6,7},
	{8,4,4}
};

int matrix2[3][3]={
	{4,8,1},
	{7,5,3},
	{6,4,2}
};

int main()
{
	printf("The addition of matrixes:\n");
	getAdd();

	printf("The multiplication of matrixes:\n");
	getMultiply();
}

void getAdd()
{
	for(int x=0;x<3;x++)
	{
		for(int y=0;y<3;y++)
		{
			printf("%d\t",matrix1[x][y]+matrix2[x][y]);
		}

		printf("\n");
	}

	printf("\n");


}

void getMultiply()
{
	int matrixMulti[3][3];

	for(int x=0;x<3;x++)
	{
		for(int y=0;y<3;y++)
		{
			matrixMulti[x][y]=0;

			for(int z=0;z<3;z++)
			{
				matrixMulti[x][y]=matrixMulti[x][y]+matrix1[x][z]*matrix2[z][y];
			}
			
			printf("%d\t", matrixMulti[x][y]);
		}

		printf("\n");
	}

}
